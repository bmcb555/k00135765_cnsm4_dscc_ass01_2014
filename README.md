# K00135765, Brian Mc Bride, CNSM4 #
## README ##
Run server.py within Vagrant virtual box (Ubuntu 14.10) and run client.py on host machine.

### What is this repository for? ###
This repository will start a tcp/ip session with the guest machine. It will accept a string 'Hello World' which can be changed by user. The message will appear on the server CLI as proof message has reached server. The server will then show the client IP that queried the server and show the time it queried the server. The IP, time and the message sent by the client will be placed in a text file messagestore1. Each new connection will add a new line to messagestore e.g

Connection: x.x.x.x
Time : x.x.x.x
Message: xxxxx

Connection: x.x.x.x
Time : x.x.x.x
Message: xxxxx

This allows for anyone to see any connection that was made. Please note the server will ony accept one connection but this can be changed.

### How do I get set up? ###

* Install Vagrant
* Install supported version of VirtualBox
* Download Ubuntu 14.10 box for Vagrant and edit the call name to "utopic"
* Clone files to a folder/directory
* Edit shared folder in the Vagrant file to the directory you created. In my code it's "/home/brian/Documents/Vagrant/k00135765_cnsm4_dscc_ass01_2014" alter to your directory
* Vagrant up from directory
* Vagrant ssh into the Ubuntu 14.10 box and cd /Vagrant to access shared folder
* Run python server.py on the Ubuntu machine
* Run python client.py from host machine

### Additional Notes ###
The reason that shared folders has been altered from default is due to issues I had after cloning from Windows to Ubuntu. The shared folder stopped working. Setting a static shared folder aloud me to keep it consistent.  